#!/system/bin/sh

t=$1

[ "X$t" == "X" ] && exit 1

mkdir /data/libs_${t}
grep -E "/(bin|lib|system|vendor).*/.*" -o /proc/$(pidof ${t})/maps | sort | uniq | while read a; do
mkdir -p /data/libs_${t}/$(dirname $a)
cp -Rv $a /data/libs_${t}/$(dirname $a)
done
